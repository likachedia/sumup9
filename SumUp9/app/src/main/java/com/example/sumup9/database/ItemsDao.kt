package com.example.sumup9.database

import androidx.room.*
import com.example.sumup9.model.ItemsItem
import kotlinx.coroutines.flow.Flow


@Dao
interface ItemDao {
    @Query("SELECT * FROM item ORDER BY uid ASC")
    fun getAll(): Flow<List<ItemsItem>>

 /*   @Query("SELECT * FROM item WHERE last_name = :stopName ORDER BY uid ASC")
    fun getByStopName(stopName: String): Flow<List<Item>> */

   /* @Query("SELECT * FROM item")
    fun getAll(): List<Item> */

    @Query("SELECT * FROM item WHERE uid IN (:userIds)")
    fun loadAllByIds(userIds: IntArray): List<ItemsItem>

 /*   @Query("SELECT * FROM user WHERE first_name LIKE :first AND " +
            "last_name LIKE :last LIMIT 1")
    fun findByName(first: String, last: String): Item */

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg items: ItemsItem)

    @Delete
    fun delete(vararg item: ItemsItem)

    @Query("DELETE FROM item")
    fun deleteAll()
}