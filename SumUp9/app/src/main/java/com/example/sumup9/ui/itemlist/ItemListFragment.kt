package com.example.sumup9.ui.itemlist

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.GridLayoutManager
import com.example.sumup9.base.BaseFragment
import com.example.sumup9.databinding.FragmentUserListBinding
import com.example.sumup9.viewmodels.ItemViewModel
import kotlinx.coroutines.flow.collect



class ItemListFragment : BaseFragment<FragmentUserListBinding, ItemViewModel>(FragmentUserListBinding::inflate) {
    override fun getViewModelClass() = ItemViewModel::class.java
    override var useSharedViewModel = true
    private lateinit var itemListAdapter: ItemListAdapter
    private lateinit var concatAdapter: ConcatAdapter
    override fun start() {

        initRecycler()
        observe()
    }

    private fun initRecycler() {
        itemListAdapter = ItemListAdapter()
        concatAdapter = ConcatAdapter(itemListAdapter)
        binding.recycler.apply {
            layoutManager = GridLayoutManager(requireContext(), 2)
            adapter = concatAdapter
        }
    }

    private fun observe(){

        if(checkForInternet(requireContext())) {
            viewLifecycleOwner.lifecycleScope.launchWhenStarted {
                repeatOnLifecycle(Lifecycle.State.STARTED) {
                    viewModel.myResponse.collect {
                        if(!it.items.isNullOrEmpty()) {
                            itemListAdapter.setData(it.items.toMutableList())
                        }
                    }
                }

            }
        } else {
            viewLifecycleOwner.lifecycleScope.launchWhenStarted {
                repeatOnLifecycle(Lifecycle.State.STARTED) {
                    viewModel.users.collect {
                        if(!it.isNullOrEmpty()) {
                        itemListAdapter.setData(it.toMutableList()) }
                    }
                }
            }

        }



    }



    private fun checkForInternet(context: Context): Boolean {

        // register activity with the connectivity manager service
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        // if the android version is equal to M
        // or greater we need to use the
        // NetworkCapabilities to check what type of
        // network has the internet connection
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            // Returns a Network object corresponding to
            // the currently active default data network.
            val network = connectivityManager.activeNetwork ?: return false

            // Representation of the capabilities of an active network.
            val activeNetwork = connectivityManager.getNetworkCapabilities(network) ?: return false

            return when {
                // Indicates this network uses a Wi-Fi transport,
                // or WiFi has network connectivity
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true

                // Indicates this network uses a Cellular transport. or
                // Cellular has network connectivity
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true

                // else return false
                else -> false
            }
        } else {
            // if the android version is below M
            @Suppress("DEPRECATION") val networkInfo =
                connectivityManager.activeNetworkInfo ?: return false
            @Suppress("DEPRECATION")
            return networkInfo.isConnected
        }
    }


}