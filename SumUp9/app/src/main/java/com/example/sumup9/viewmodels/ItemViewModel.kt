package com.example.sumup9.viewmodels

import android.util.Log
import androidx.lifecycle.viewModelScope
import com.example.sumup9.base.BaseViewModel
import com.example.sumup9.database.AppDatabase
import com.example.sumup9.model.ItemsItem
import com.example.sumup9.resource.Resource
import com.example.sumup9.util.Repository
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch



class ItemViewModel(private  val repository: Repository): BaseViewModel(repository) {


    val users: Flow<List<ItemsItem>> = AppDatabase.db.itemDao().getAll().catch {
    }

    private val _myResponse: MutableStateFlow<UiState> = MutableStateFlow(UiState(error = UiState.Error.ClientError))
    val myResponse: MutableStateFlow<UiState> get() = _myResponse


    private val _uiState = MutableSharedFlow<UserUiState>()
    val uiState: MutableSharedFlow<UserUiState> = _uiState


    private fun insertData(list: List<ItemsItem>) {
        if(list.isNotEmpty()) {

            viewModelScope.launch(IO) {
                AppDatabase.db.itemDao().deleteAll()
                AppDatabase.db.itemDao().insertAll(*list.toTypedArray())
               /// _uiState.emit(UserUiState.Success(users))
            }
        }
    }
    init {
        fetchData()
    }


    private fun fetchData() {
        viewModelScope.launch {
            val data = repository.getPost()
            Log.i("data", data.data.toString());
            when(data) {
                is Resource.Success -> {
                    _myResponse.value = UiState(items = (data.data))
                    // Log.i("uistate-1", _uiState.toString())
                   insertData(data.data!!)
                }
                is Resource.Error -> {
                    _myResponse.value = UiState(error = UiState.Error.ClientError)
                }
            }
        }
    }

    data class UiState(
        val isLoading: Boolean = false,
        val error:Error? = null,
        val items: List<ItemsItem>? = null
    ) {
        sealed class Error {
            object ClientError: Error()
        }
    }

    sealed class UserUiState {
        data class Success(val items: List<ItemsItem>): UserUiState()
        data class Error(val exception: Throwable): UserUiState()
    }
}