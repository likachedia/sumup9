package com.example.sumup9.util

import android.util.Log
import com.example.sumup9.model.ItemsItem
import com.example.sumup9.network.RetrofitInstance
import com.example.sumup9.resource.Resource
import okio.IOException
import retrofit2.HttpException

class Repository {


    suspend fun getPost(): Resource<List<ItemsItem>> {
        return try {
            val response = RetrofitInstance.api.getCustomPost()
            Log.i("response: ", response.toString())
            val result = response.body()
            if(response.isSuccessful && result != null) {
                Log.i("response get: ", response.toString())
                Resource.Success(result)

            } else {
                Log.i("response get: ", response.message().toString())
                Resource.Error("Something went wrong")
            }

        }catch (exception: IOException) {
            Resource.Error(exception.toString())
        } catch (exception: HttpException) {
            Resource.Error(exception.toString())
        }
    }

}