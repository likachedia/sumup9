package com.example.sumup9.util

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.sumup9.viewmodels.ItemViewModel

class ItemViewModelFactory(private val repository: Repository): ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return  ItemViewModel(repository) as T
    }
}