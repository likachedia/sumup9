package com.example.sumup9.database

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.sumup9.App.Companion.appContext
import com.example.sumup9.model.ItemsItem

@Database(entities = [ItemsItem::class], version = 2)
abstract class AppDatabase: RoomDatabase() {
    abstract fun itemDao(): ItemDao


    companion object {
        val db by lazy {
            Room.databaseBuilder(
                appContext,
                AppDatabase::class.java, "item"
            ).fallbackToDestructiveMigration().build()
        }
    }

}