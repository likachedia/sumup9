package com.example.sumup9.model


import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.Json

@Entity(tableName = "item")
data class ItemsItem(

    @Json(name = "cover")
    val cover: String?,
    @Json(name = "liked")
    val liked: Boolean?,
    @Json(name = "price")
    val price: String?,
    @Json(name = "title")
    val title: String?
) {
    @PrimaryKey(autoGenerate = true) var uid: Int = 0
}