package com.example.sumup9.network

import com.example.sumup9.model.ItemsItem
import retrofit2.Response
import retrofit2.http.GET

interface SimpleApi {

  @GET("v3/05d71804-4628-4269-ac03-f86e9960a0bb")
    suspend fun getCustomPost(
    ): Response<List<ItemsItem>>


}