package com.example.sumup9.base


import androidx.lifecycle.ViewModel
import com.example.sumup9.util.Repository


abstract class BaseViewModel(repository: Repository): ViewModel()