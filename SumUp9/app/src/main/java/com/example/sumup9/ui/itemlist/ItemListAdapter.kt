package com.example.sumup9.ui.itemlist

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.sumup9.R
import com.example.sumup9.databinding.UserListItemBinding
import com.example.sumup9.extenstion.setImage
import com.example.sumup9.model.ItemsItem


class ItemListAdapter: RecyclerView.Adapter<ItemListAdapter.ActiveCourseViewHolder>() {
    private val items:MutableList<ItemsItem> = mutableListOf()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemListAdapter.ActiveCourseViewHolder {
        return ActiveCourseViewHolder(UserListItemBinding.inflate(LayoutInflater.from(parent.context),parent, false))
    }

    override fun onBindViewHolder(holder: ItemListAdapter.ActiveCourseViewHolder, position: Int) {
        val model = items[position]
        holder.onBind(model)
    }

    override fun getItemCount() = items.size

    inner class ActiveCourseViewHolder(private val binding: UserListItemBinding) : RecyclerView.ViewHolder(binding.root){

        fun onBind(model:ItemsItem) {

            with(binding) {
                itemImage.setImage(model.cover.toString())
                title.text = model.title.toString()
                price.text = model.price.toString()
            }

            if(model.liked == false) {
                binding.btnFavourite.setImageResource(R.drawable.ic_baseline_favorite_border_24)
            }

        }


    }
    fun setData(item: MutableList<ItemsItem>) {
        items.clear()
        items.addAll(item)
        notifyDataSetChanged()
    }
}